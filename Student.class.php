

<?php


class Student {
    private int $id;
    private $name;
    private $surname;
    private $group_number;
    private int $scores;
    private $gender;
    private $email;

    function __construct(
        $id,
        $name,
        $surname,
        $group_number,
        $scores,
        $gender,
        $email
    ){
        $this->id = $id;
        $this->name = $name;
        $this->surname = $surname;
        $this->group_number = $group_number;
        $this->scores = $scores;
        $this->gender = $gender;
        $this->email = $email;
    }

    function getStudentName(){
        return $this->name;
    }

    function getStudentSurname(){
        return $this->surname;
    }

    function setStudentName($name){
        $this->name = $name;
    }
    function setStudentSurname($surname){
        $this->surname = $surname;
    }
    

}

//$a = new Student('1', 'Sasha', 'grigirev', 'qwqd', '56','M','flaaim@list.ru');

//var_dump($a);
