<?php 
    require_once __DIR__."/Controller.php";
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style/style.css">
    
    <title>List of Students</title>
</head>
<body>
    <table>
        <tr>
            <td>Имя</td>
            <td>Фамилия</td>
            <td>Группа</td>
            <td>Баллы</td>
            <td>Пол</td>
            <td>Email</td>
        </tr>
        
        <?php   

                 foreach(showStudents($output->showStudents()) as $outputShowStudents){
                    $studentTable = "";
                    $studentTable .= "<tr>";
                    $studentTable .= "<td>".$outputShowStudents->getStudentName()."</td>";
                    $studentTable .= "<td>".$outputShowStudents->getStudentSurname()."</td>";
                    $studentTable .= "</tr>"; 
                    
                    echo $studentTable;
                     
                }    
        ?>
    </table>
</body>
</html>