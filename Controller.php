<?php

require_once __DIR__."/Helper.class.php";
require_once __DIR__."/Student.class.php";

 

function showStudents($rows){
    foreach($rows as $row){
        $student = new Student(
            $row['id'],
            $row['name'],
            $row['surname'],
            $row['group_number'],
            $row['scores'],
            $row['gender'],
            $row['email']
        );
        $arr[] = $student;
    }
        return $arr; 
}


