<?php
    
    require_once __DIR__."/DB.class.php";
    require_once __DIR__."/Student.class.php";
    class StudentsTableGateway{
            protected $conn;

            public function __construct(PDO $connection){
                $this->conn = $connection;
            }


            public function showStudents(){
                $sql = "SELECT * FROM students";
                $statement = $this->conn->prepare($sql);
                $statement->execute();
                
               
                $rows = $statement->fetchAll(PDO::FETCH_ASSOC);
                return $rows;
                     
            }
    }


$output = new StudentsTableGateway($connection);

$output->showStudents();
